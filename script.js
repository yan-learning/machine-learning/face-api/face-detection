const video = document.getElementById('video');
const video2 = document.getElementById('video2');
const imageUpload = document.getElementById('imageUpload');

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('./models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('./models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('./models'),
    faceapi.nets.faceExpressionNet.loadFromUri('./models'),
    faceapi.nets.ssdMobilenetv1.loadFromUri('./models'),
]).then(startVideo);


async function startVideo(){
    navigator.getUserMedia({video:{}}, stream=>video.srcObject = stream, err=>console.log(err) );
    document.body.append("Loaded");
    const container = document.createElement('div');
    const labeledFaceDescriptors = await loadLabelImages();
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);
    let image, canvas

    container.style.position = "relative";
    document.body.append(container);
    imageUpload.addEventListener("change", async()=>{
        
        if(image) image.remove();
        if(canvas) canvas.remove();
        image = await faceapi.bufferToImage(imageUpload.files[0])
        canvas = await faceapi.createCanvasFromMedia(image);
        container.append(image);
        container.append(canvas);
        const displaySize = {width:image.width, height: image.height};
        faceapi.matchDimensions(canvas, displaySize);
        const detections = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors();
        document.body.append(detections.length);
        const resizedDetections = faceapi.resizeResults(detections, displaySize);
        const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor));
        results.forEach((result, i) => {
            const box = resizedDetections[i].detection.box;
            const drawBox = new faceapi.draw.DrawBox(box,{
                label:result.toString()
            });
            drawBox.draw(canvas);
        })
    });
}

function loadLabelImages(){

    const labels = ['Black Widow', 'Captain America', 'Captain Marvel', 'Hawkeye', 'Tony Stark', 'Thor', 'Jim Rhodes', 'Tony Stark', "Chris Pratt", "Hulk"]
    
    return Promise.all(
        labels.map(async label => {
            const descriptions = [];
            for(let i=1; i<=2; i++){
                try{
                    const img = await faceapi.fetchImage(`http://localhost:5500/labeled_images/${label}/${i}.jpg`);
                    const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
                    descriptions.push(detections.descriptor);
                }catch{}
            }
            return new faceapi.LabeledFaceDescriptors(label, descriptions);
        })
    );

}



//startVideo();
video.addEventListener("play", ()=>{
    const canvas =faceapi.createCanvasFromMedia(video);
    const container = document.getElementById('face-detection');
    container.style.position = "relative";
    container.append(canvas);
    const displaySize = {width: video.width, height: video.height};
    faceapi.matchDimensions(canvas, displaySize);
    setInterval(async()=>{
        const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions();
        console.log(detections);
        const resizedDetections = faceapi.resizeResults(detections, displaySize);
        canvas.getContext('2d').clearRect(0,0, canvas.width, canvas.height);
        faceapi.draw.drawDetections(canvas, resizedDetections);
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
        faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
    }, 100)
});
